
from mysound import Sound


class SoundSin(Sound):
    def __init__(self, duration, frequency, amplitude):
        super().__init__(duration)
        self.frequency = frequency
        self.amplitude = amplitude
        self.sin(self.frequency, self.amplitude)
        print(Sound.bars(self, bar_period=0.0001))


sin = SoundSin(1, 440, 10000)
