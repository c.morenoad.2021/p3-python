from mysound import Sound


def soundadd(s1: Sound, s2: Sound) -> Sound:
    samples1 = s1.buffer
    samples2 = s2.buffer

    # Obtener la longitud máxima entre ambos sonidos
    max_length = max(len(samples1), len(samples2))

    # Crear un sonido resultante con la longitud máxima
    sonido = Sound(max_length)

    # Sumar las muestras correspondientes
    for i in range(len(sonido.buffer)):
        if i < len(samples1) and i < len(samples2):
            sonido.buffer[i] = samples1[i] + samples2[i]
        else:
            if i < len(samples1):
                sonido.buffer[i] = samples1[i]
            else:
                sonido.buffer[i] = samples2[i]

    return sonido.bars(0.1)


s1 = Sound(1)
s1.sin(440, 1000)
s2 = Sound(1)
s2.sin(880, 1000)
print(soundadd(s1, s2))
